
from __future__ import print_function
import requests
import time
import json
import traceback

import furaffinity

import Networking

#==============#
# Custom Error

class RemoteServerError (Exception):
	"""
	Something went wrong on the server side.
	"""
	pass

#==============#
# Worker Class

class FurAffinityScraperWorker:

	image_not_found_hash = "\xfb\xe9\xb8\x72\x7e\x0a\xe2\x4b\xaa\xcf\x63\xb6\x55\x3d\x33\x1c\x84\x77\x9e\x40\xb7\x43\x38\x06\x28\xa5\x18\x1e\x0e\x9f\xa2\xff"
	version = 4		# Arbitrary version numbers!

	def __init__ ( self ):
		print("Starting scraper worker... ")

		# Set up basic settings
		self.blockSize = 100

		# Set up session
		self.session = furaffinity.Furaffinity()

		# Set up networking
		self.network = None

		self.blockHashMap = []
		self.waitingBlocks = set()

		self.currentBlock = None
		self.currentSubmission = None
		self.endOfBlock = None
		self.submissionTries = 0

		self.maximumSubmissionTries = 10

		self.idle = False

		self.scrapeInterval = 0
		self.lastScrape = 0
		self.debuggingMode = False

		self.username = "None"
		self.password = "None"

	def SendRequest ( self ):
		msg = self.CreateMessage(
			msgType="request"
		)

		self.SendMessage( msg )

	def SetWorkerInfo ( self, username, password ):
		self.username = username
		self.password = password

	def ConnectToFuraffinity ( self, username, password, useragent ):
		self.session.SetUseragent(useragent)
		self.session.Login(username,password)

		self.session.SetAccountSettings(timezone="+0000",timezone_dst="0",viewmature="1")

	def Update ( self ):
		while True:
			msg = self.RecvMessage()
			if msg:
				self.ReceivedMessage( msg )
				self.idle = 0
			else:
				break

		scrapeStatus = self.UpdateScrape()

		if not scrapeStatus:
			time.sleep(0.2)
			if not self.idle:
				self.idle = 1
				self.SendRequest()
				print("Currently idleling at",time.time(),"UNIX time")
			else:
				self.idle += 1
				if self.idle > 100:
					self.idle = 1
					self.SendRequest()

	def UpdateScrape( self ):
		if not self.currentBlock:
			return False

		hash_and_tags = None
		try:
			hash_and_tags = self.ScrapePage( self.currentSubmission )
		except furaffinity.IpBanError:
			raise
		except (requests.ConnectionError,furaffinity.AccessError) as error:
			print("Error happened. Need a retry:",self.submissionTries)
			if self.submissionTries < self.maximumSubmissionTries:
				self.submissionTries += 1
				return True
			else:
				self.BadBlock( error, traceback.format_exc() )
				return self.SetNextBlock( True )
		except Exception as error:
			if self.debuggingMode:
				error.blockID = self.currentBlock
				error.submissionID = self.currentSubmission
				raise
			else:
				self.BadBlock( error, traceback.format_exc() )
				return self.SetNextBlock( True )
		else:
			if hash_and_tags:
				self.blockHashMap.append(hash_and_tags)

			self.currentSubmission += 1
			self.submissionTries = 0
			if self.currentSubmission > self.endOfBlock:
				self.SendBlock( self.currentBlock, self.blockHashMap )
				print("Block",self.currentBlock,"complete.")
				self.SetNextBlock()
				self.blockHashMap = []
			return True

	def ReceivedMessage ( self, msg ):
		if msg.get("msgType") == "serverError":
			print("Server encountered a problem. Shutting down worker.")
			raise RemoteServerError

		elif msg.get("username") == self.username and msg.get("msgType") == "assign":
			print("Blocks received:",msg.get("blocks"))
			for block in msg.get("blocks"):
				if block != self.currentBlock:
					self.waitingBlocks.add(block)

			if self.currentBlock == None:
				self.SetNextBlock()

		elif msg.get("username") == self.username and msg.get("msgType") == "ping":
			reMsg = self.CreateMessage(
				msgType="unping"
			)
			self.network.SendMessage( reMsg )

	def BadBlock( self, error, errorTraceback ):
		print("Encountered", type(error).__name__, "error at id", self.currentSubmission, "in block", self.currentBlock, ":\n", errorTraceback)
		msg = self.CreateMessage(
			msgType="badBlock",
			info=(self.currentBlock,self.currentSubmission,type(error).__name__,errorTraceback)
		)
		self.SendMessage( msg )

	def SetNextBlock ( self, force=False ):
		if self.currentSubmission > self.endOfBlock or self.currentBlock == None or force:
			if len(self.waitingBlocks)>0:
				self.currentBlock = self.waitingBlocks.pop()
				self.currentSubmission = self.currentBlock*self.blockSize+1
				self.endOfBlock = (self.currentBlock+1)*self.blockSize
				print("Moving onto block",self.currentBlock)
			else:
				self.currentBlock = None
				print("No more blocks in queue...")

		return not not self.currentBlock

	# Returns true if the image was scraped, false if not
	def ScrapePage( self, pageId ):
		try:
			furPage = self.session.GetPage( pageId )
		except furaffinity.NonExistantSubmissionError:
			return None

		try:
			hash_ = furPage.GetHash()
		except furaffinity.SubmissionFileNotAccessible:
			return None
		if hash_ == self.image_not_found_hash:
			return None

		tags = furPage.GetKeywords()
		tags.append( u"title:" + furPage.GetTitle() )
		tags.append( u"creator:" + furPage.GetUploader() )

		gender = furPage.GetGender()
		if gender:
			tags.append( u"gender:" + gender )

		species = furPage.GetSpecies()
		if species:
			tags.append( u"species:" + species )

		# Things that really only makes sense in context of FA.
		tags.append( u"f_id:" + str(pageId) )
		tags.append( u"f_theme:" + furPage.GetTheme() )
		tags.append( u"f_category:" + furPage.GetCategory() )
		tags.append( u"f_age_rating:" + furPage.GetAgeRating() )
		tags.append( u"f_upload_time:" + furPage.GetUploadTime() )

		for username in furPage.GetTaggedUsers():
			tags.append( u"f_tagged_user:" + username )

		return { "hash":hash_, "tags":tags }

	def SendBlock ( self, blockID, hashList ):
		msg = self.CreateMessage(
			msgType="mappings",
			hashes=hashList,
			block=blockID
		)

		self.SendMessage( msg )
		
	def SendMessage ( self, msg ):
		return self.network.SendMessage( msg )

	def RecvMessage ( self ):
		return self.network.RecvMessage()

	def Stop ( self, reason="", sendMessage=True ):
		if sendMessage:
			msg = self.CreateMessage(
				msgType="goodbye",
				reason=reason
			)
			self.network.SendMessage( msg )

		time.sleep(1)
		self.network.Close()
		print("Closing down:\n",reason)

	def CreateMessage ( self, msgType, **kwargs ):
		msg = {
			"msgType": msgType,
			"username": self.username,
			"password": self.password,
			"version": self.version
		}

		for key, value in kwargs.iteritems():
			msg[key] = value

		return msg

	def LoadInfo ( self ):
		try:
			with open("conf.json","r") as configFile:
				config = json.loads( configFile.read() )
				self.network = Networking.Networking( config.get("connAddr") + ":" + config.get("connInPort"), config.get("connAddr") + ":" + config.get("connOutPort") )
				self.ConnectToFuraffinity( config.get("furLogin"), config.get("furPass"), config.get("userAgent") )
				self.SetWorkerInfo( config.get("username"), config.get("password") )

				maxSubmissionsPrHour = config.get("maxSubmissionsPrHour") or 1000
				self.scrapeInterval = maxSubmissionsPrHour and 3600.0 / maxSubmissionsPrHour or 0

				print("Configuration loaded...")

		except IOError:
			print("No configuration file found. Creating one now...")
			print("To use the worker, open the 'conf.json' file, and do some configuration. Then run the worker again.")
		
			with open("conf.json","w") as configFile:
				baseConf = {
					"username": "USERNAME HERE",
					"password": "PASSWORD HERE",

					"connAddr": "tcp://127.0.0.1",
					"connInPort": "12321",
					"connOutPort": "32123",
		
					"furLogin": "FURAFFINITY USERNAME HERE",
					"furPass": "FURAFFINITY PASSWORD HERE",
					"userAgent": "CUSTOM USERAGENT HERE, MAKE IT DESCRIPTIVE",

					"maxSubmissionsPrHour": 1000
				}
				configFile.write( json.dumps( baseConf, indent=4 ) )
			exit()

if __name__ == "__main__":

	bestWorker = FurAffinityScraperWorker()
	bestWorker.LoadInfo()

	try:
		while True:
			bestWorker.Update()
	except KeyboardInterrupt as error:
		bestWorker.Stop( "shutting down" )
	except furaffinity.MaturityError:
		print("Cannot scrape properly without access to adult and mature content.\nUpdate your settings before you try to scrape again!")
		bestWorker.Stop( "maturity filter" )
	except furaffinity.IpBanError:
		print("Your IP appears to be banned. Not much I can do about it.")
		bestWorker.Stop( "ip ban" )
	except Exception as error:
		info = [type(error).__name__,traceback.format_exc()]
		if hasattr(error, 'submissionID'):
			info[1] += " at submission "+str(error.submissionID)
		bestWorker.Stop( info, type(error).__name__!="RemoteServerError" )
		raise

