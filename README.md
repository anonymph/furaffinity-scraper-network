# Furaffinity Hash&Tag Scraper Network-Thing

This is a set of utility scripts designed to interact with the Furaffinity website and the Hyrdus Network program.

A central server delegates which submissions should be scraped, while workers go through and scrape the actual pages.

## Dependencies
Requires the following modules:

- ZeroMQ
- MessagePack
- requests
- BeautifulSoup

Just download them through pip with the command:

    pip install u-msgpack-python requests beautifulsoup4 pyzmq

If you encounter an error involving something like `fatal error: Python.h`, you are missing the python header files. On debian/ubuntu they can be downloaded with:

    apt-get install python-dev

Search a little around if you aren't on debian/ubuntu.

I have only tested the scripts in python 2.7.8, and I am not sure if they workin in python 3. If anyone could confirm or deny this, let me know.

## Installing
It is highly recommended to download through git, as it allows you to easily update.
Use the command below on any system with git support:
	
	git clone https://anonymph@bitbucket.org/anonymph/furaffinity-scraper-network.git

You can also download the repository as a zip file, but then you wouldn't be able to use this glorious command to update:

	git pull

## How to set up

### Server
Run `Server.py` once, this will create the `serverState.json` file. Open the file, add some users, change the block range, save and run `server.py` again.

### Worker
Run `Worker.py` once to create the `conf.json` config file.
Open the file, change some settings. Some tips on these settings:
- Everything should be throwaway accounts. The server admin cannot see your furaffinity username or password, but s/he can see and should know the username and password you use to communicate with the server. Avoid using any of your standard usernames or passwords.
- 'useragent' should be as standard a useragent as possible. This can be hard to do by hand, either find out what your standard browser uses, or find a random one from [this site](http://www.useragentstring.com/pages/useragentstring.php).
- 'maxSubmissionsPrHour' is not used current, but will in the future place a limit upon how often you are able to scrape pages. Set to '0' to disable this feature.
- 'connAddr', 'connOutPort' and 'connInPort' will all be given to you by the server admin. In general 'connAddr' will always start with "tcp://".
- You will need to alert the server admin to the fact that you want to help. Send her/him a message with your 'username' and 'password' settings.

## Server
I am currently hosting a server at `178.62.124.54`.
To get access to this server, you should email me at `anonymph@8chan.co` with the desired username and password.

I am currently looking for a method to distribute these large database files.

## To Do
These are things I am currently thinking of doing:

- Make both the server and workers more robust against repeated messages.
- I want to rewrite `HydrusTagArchive.py` to better fit the style I am working in, and to allow certain improvements.
- I plan to write some stuff to add support for python 3.

## License
Everyhing is licensed under the "DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE", allowing you to do whatever the fuck you want to do.
