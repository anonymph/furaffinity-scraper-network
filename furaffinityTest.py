# furaffinity.py test file.
# This file is meant to do some tests to make sure the furaffinity.py library works properly.

from __future__ import print_function
import json
import traceback
import binascii
import os

import furaffinity

### TEST FUNC ###

TESTING_IMMEDIATE_FEATURE = False

def stop():
	print("\n\nCould not continue with the test:\n",traceback.format_exc())
	exit()

### CONFIGURATION ###

print("Loading config ...")

session = furaffinity.Furaffinity()

conf = {
	"furLogin": None,
	"furPass": None,
	"userAgent": None
}

try:
	with open("conf.json") as configRaw:
		conf = json.loads(configRaw.read())
	print("\tLoaded.")
except IOError:
	print("\tNot found.")
	conf["furLogin"] = raw_input("Furaffinity Loginname: ")
	conf["furPass"] = raw_input("Furaffinity Password: ")
	conf["userAgent"] = raw_input("Useragent: ")

session.SetUseragent(conf["userAgent"])

### FALSE LOGIN ###

if not TESTING_IMMEDIATE_FEATURE:
	print("Logging in incorrectly.")

	try:
		session.Login(conf["furLogin"],conf["furPass"]+" Bad Password")
	except furaffinity.LoginError as error:
		print("\tError: ",error)
		print("\tCatched correctly")
	else:
		print("\tNo error. WRONG.")
		stop()

### PROPER LOGIN ###

print("Logging in correctly.")

try:
	session.Login(conf["furLogin"],conf["furPass"])
except furaffinity.LoginError as error:
	print("\tError:",error)
	stop()
else:
	print("\tLogged in.")


### DOING IMMEDIATE TESTING OF FEATURE ###

# INSERT NEW FEATURE HERE

if TESTING_IMMEDIATE_FEATURE:
	exit()

### MAKING SURE MATURITY SETTINGS WORK ###

print("Testing maturity settings")

for (mature,matureNr) in (("General",0),("Mature",2),("Adult",1)):
	session.SetAccountSettings( viewmature=matureNr )
	print("\t",mature)
	for (mature1,pageId) in (("General",16226482),("Mature",15894565),("Adult",15895239)):
		try:
			session.GetPage(pageId)
		except:
			print("\t\t",mature1,": No")
		else:
			print("\t\t",mature1,": Yes")

### FETCHING A PAGE AND PROCESSING IT ###

def FetchPage(f_id):
	page = session.GetPage(f_id)
	
	print("Fetched page with id",f_id,":")

	print("\tSubmission:", page.GetTitle() )
	print("\tBy:", page.GetUploader() )

	print("\tFilename:", page.GetFileName() )
	print("\tFound at:", page.GetDownloadUrl() )

	print("\tAge rating:", page.GetAgeRating() )

	print("\tHash:", binascii.hexlify(page.GetHash()) )

	print("\tTime:" )
	print("\t\tNative:", page.GetRawTime() )
	print("\t\tDatetime:", page.GetDatetime() )
	print("\t\tHeader:", page.GetUploadTime() )

	print("\tCategory:", page.GetCategory() )
	print("\tTheme:", 	page.GetTheme() )
	print("\tSpecies:", 	page.GetSpecies() )
	print("\tGender:", 	page.GetGender() )

	print("\t# Favorites:",	page.GetFavorites() )
	print("\t# Comments:",	page.GetComments() )
	print("\t# Views:",		page.GetViews() )

	print("\tKeywords:" )
	for keyword in page.GetKeywords():
		print("\t\t",keyword)

	print("\tTagged users:" )
	for user in page.GetTaggedUsers():
		print("\t\t",user)

	#	Missing:
	#		Getting the Comments.
	#		Getting the description.

FetchPage(16226482)

### TRYING OUT THE TAG SEARCH FUNCTION ###

print("Using the sites search function.")
print("\tSearch: '@keywords Tiefling'")
print("\tResults:")
print("\t\t",session.GetTaggedSubmissions("Tiefling"))

### TRYING OUT THE ARTIST GALLERY FUNCTION ###

print("Getting submission ids from artist: 'shysiren'")
print("\tResults:")
print("\t\t",session.GetUserGallery("shysiren"))

### TRYING OUT RETRIVAL OF WATCHLIST ###

print("Getting users on user's watchlist.")
print("\tResults:")
print("\t\t",session.GetWatchlist())
