"""
Module used to communicate between server and worker programs.

Classes:
	Networking
	NetworkingServer

Libs used:
	zmq
	zlib
	umsgpack
"""

import zmq
import zlib
import umsgpack as msgpack

class Networking:
	"""
	Used to connect to a NetworkingServer object, from a worker program.
	"""

	def __init__ (self, incomingAddress, outgoingAddress):
		"""
		Initiates the Networking object.
		"""

		# Set up zeromq
		self.zmq_context = zmq.Context()
		
		self.incomingAddress = incomingAddress
		self.incoming = self.zmq_context.socket( zmq.SUB )
		self.incoming.connect( incomingAddress )
		self.incoming.setsockopt( zmq.SUBSCRIBE, '' )
		
		self.outgoingAddress = outgoingAddress
		self.outgoing = self.zmq_context.socket( zmq.PUB )
		self.outgoing.connect( outgoingAddress )
		self.outgoing.setsockopt(zmq.LINGER, 100)

	def SendMessage ( self, message ):
		"""
		Sends a message to the outgoing address.
		The message should be a datatype that msgpack can encode, and should be a tree graph, not mesh.
		"""
		encodedMessage = self.EncodeMessage( message )
		self.outgoing.send( encodedMessage )

	def RecvMessage ( self ):
		"""
		Checks for a message, and returns it if it exist.
		"""
		try:
			encodedMessage = self.incoming.recv(flags=zmq.NOBLOCK)
			decodedMessage = self.DecodeMessage( encodedMessage )
			return decodedMessage
		except zmq.error.Again:
			return None

	def EncodeMessage ( self, message ):
		"""
		Encodes a message.
		"""
		packedMessage = msgpack.packb( message )
		compressedMessage = zlib.compress( packedMessage )
		return compressedMessage
	
	def DecodeMessage ( self, encodedMessage ):
		"""
		Tries to decode a message, and return the value.
		If i can't, return None.
		"""
		try:
			uncompressedMessage = zlib.decompress( encodedMessage )
			unpackedMessage = msgpack.unpackb( uncompressedMessage )
			return unpackedMessage
		except (zlib.error, msgpack.UnpackException) as error:
			return None

	def Close ( self ):
		"""
		Disconnects the sockets and destroys the zmq context.
		"""
		self.incoming.disconnect(self.incomingAddress)
		self.outgoing.disconnect(self.outgoingAddress)
		self.zmq_context.destroy()

class NetworkingServer( Networking ):
	"""
	The server side to the Networking object.
	"""

	def __init__ ( self, incomingAddress, outgoingAddress ):
		"""
		Initiates the NetworkingServer object.
		"""

		# Set up zeromq
		self.zmq_context = zmq.Context()

		self.incomingAddress = incomingAddress
		self.incoming = self.zmq_context.socket( zmq.SUB )
		self.incoming.bind(incomingAddress)
		self.incoming.setsockopt(zmq.SUBSCRIBE,'')
		self.incomingAddress = incomingAddress

		self.outgoingAddress = outgoingAddress
		self.outgoing = self.zmq_context.socket( zmq.PUB )
		self.outgoing.bind(outgoingAddress)
		self.outgoing.setsockopt(zmq.LINGER, 100)

	def Close ( self ):
		"""
		Unbinds the sockets and destroys the zmq context.
		"""
		self.incoming.unbind(self.incomingAddress)
		self.outgoing.unbind(self.outgoingAddress)
		self.zmq_context.destroy()
