"""
Library for seperating the backend and frontend of an application into two parts.
"""

from __future__ import print_function
import threading
import pickle
import time
import re
import traceback

import zmq

CONTEXT = zmq.Context()

def _availableToFrontend( returns=True ):
	"""
	Decorator, to make a function available across threads.
	"""
	def decorator ( func ):
		func.interfaceAvailable = True
		func.returnToInterface = returns
		return func
	return decorator

class Backend (object):

	def __init__ (self):
		pass

	def Close (self):
		print("Shutting down")
	
	#########################

	def Update (self):
		pass

	#########################

	@_availableToFrontend(False)
	def Print (self, *args):
		print( *args )

class BackendThread ( threading.Thread ):

	def __init__ (self,backend,address):
		super(BackendThread, self).__init__(name="Database Thread")

		self.socket = CONTEXT.socket( zmq.PAIR )
		self.socket.bind( address )

		self.backend = backend

		self.alive = True
		self.daemon = True

	def run (self):
		self.backend.Start()
		while self.alive:
			self.backend.Update()
			self.RecvMsg()

	def SendMsg ( self, msg ):
		#print("Sending message: ",msg)
		packed = pickle.dumps( msg )
		self.socket.send( packed )
		#print("Done sending messages")

	def RecvMsg ( self ):
		try:
			packedMsg = self.socket.recv( flags=zmq.NOBLOCK )
			msg = pickle.loads( packedMsg )
			if msg["func"] == "Close":
				self.Close()
			else:
				self.ProcessMsg( msg )
		except zmq.error.Again:
			pass

	def ProcessMsg ( self, msg ):
		method = hasattr( self.backend, msg["func"] ) and getattr( self.backend, msg["func"] )
		returnMsg = {"result":None,"func":msg["func"],"done":0}
	
		if method and hasattr(method,"interfaceAvailable"):
			try: 
				return_ = method( *msg["args"] )
				if method.returnToInterface:
					returnMsg["result"] = return_
			except TypeError as error_:
				print( repr(error_) )
				returnMsg["done"] = 2
			except Exception as error:
				returnMsg["done"] = 3
				returnMsg["error"] = traceback.format_exc()
		else:
			returnMsg["done"] = 1
		
		self.SendMsg( returnMsg )

	def Close (self):
		self.backend.Close()
		self.socket.close()
		self.alive = False

class Frontend (object):
	"""
	Used to pass function calls and arguments to the actual Backend thread.
	"""
	
	def __init__(self,address):

		self.socket = CONTEXT.socket( zmq.PAIR )
		self.socket.connect(address)
		self.alive = True

	def SendMsg ( self, msg ):
		packed = pickle.dumps( msg )
		self.socket.send( packed )

	def RecvMsg ( self ):
		try:
			packedMsg = self.socket.recv()
			return pickle.loads( packedMsg )
		except zmq.error.Again:
			return None

	def __getattr__ ( self, varName ):
		"""
		When any function is called from InterfaceClass, everything regarding the call is passed to Actual class.
		"""
		def func ( *arg ):
			self.SendMsg( {"func":varName,"args":arg} )
			returns = self.RecvMsg()
			if returns["done"]==1:
				print("Function not found.")
			elif returns["done"]==3:
				print("Error under execution of function",returns["func"],":\n",returns["error"])
			return returns["result"]
		return func

	def Close ( self ):
		self.SendMsg( {"func":"Close","args":[]} )
		time.sleep(0.2)
		self.CloseInterface()

	def CloseInterface ( self ):
		self.alive = False
		self.socket.close()

	def __del__ ( self ):
		if self.alive:
			self.Close()

	def __enter__ (self):
		return self

	def __exit__ (self, exception_type, exception_val, trace):
		if self.alive:
			self.CloseInterface()

########################

class CLIFrontend( Frontend ):
	"""
	Quick implementation of a command line interface, with a background/backend process doing stuff.
	"""

	def Start ( self ):
		while self.alive:
			try:
				input_ = raw_input("!: ")
				func_and_args = re.findall( "((?:\"|\')[^\"]+(?:\"|\')|[^ \t]+)", input_ )
				if len(func_and_args)>0:
					args = filter(None,func_and_args[1:])
					result = getattr( self, func_and_args[0] )( *args )
					if result:
						print(">",result)
			except KeyboardInterrupt:
				self.Close()
			except:
				self.Close()

########################

# Demonstration of how to employ.
if __name__ == "__main__":
	backend = Backend()
	thread = BackendThread(backend,"inproc://backendThread")
	thread.start()

	interface = CLIFrontend("inproc://backendThread")
	interface.Start()