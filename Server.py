
from __future__ import print_function
import time
import json
import os.path
import logging

import HydrusTagArchive

import Networking
import BackFront as bf

def VerifyMessage ( msg ):
	"""
	Used to validate messages.
	"""

	try:
		if msg["msgType"] == "mappings":
			valid = True
			for hash_tags in msg["hashes"]:
				valid = valid and isinstance(hash_tags["hash"],basestring) and isinstance(hash_tags["tags"],list)
				for tag in hash_tags["tags"]:
					valid = valid and isinstance(tag,basestring)
				if not valid:
					return False 
			return True
		elif msg["msgType"] == "badBlock":
			tuple_ = msg["info"]
			return isinstance(tuple_[0],int) and isinstance(tuple_[1],int) and isinstance(tuple_[2],basestring) and ( len(tuple_)<=3 or isinstance(tuple_[3],basestring))

	except (KeyError, TypeError, ValueError, IndexError) as error_: 
		return False
	else:
		return True

class FurAffinityScraperServer (bf.Backend):

	def __init__( self, incomingAddress, outgoingAddress ):

		logging.info("Starting scraper server... ")

		# Set up basic settings
		self.blockSize = 100
		self.assignedBlocksPrWorker = 5
		self.maxBlockAllocationTime = 3600
		self.cleanupInterval = 600

		# Set up worker dict
		self.workers = {}

		self.lastCleanup = 0

		# Set up block
		self.missingBlocks = []
		self.assignedBlocks = {}
		self.badBlocks = []

		self.address = (incomingAddress, outgoingAddress)

	def Start ( self ):
		# Set up database
		self.database = HydrusTagArchive.HydrusTagArchive("furAffinity.db")
		self.database.SetHashType( HydrusTagArchive.HASH_TYPE_SHA256 )

		# Set up networking
		self.network = Networking.NetworkingServer(self.address[0],self.address[1])

	def GetMissingBlockId ( self ):
		try:
			return self.missingBlocks[-1][0]
		except IndexError:
			return None

	def AssignBlock ( self, username, blockId ):
		self.assignedBlocks[ blockId ] = ( username, time.time() )
		self.RemoveFromMissing( blockId )

	def SendBlockMessage ( self, username, blockId ):
		msg = {
			"msgType":"assign",
			"username":username,
			"block":blockId
		}
		self.SendMessage( msg )

	def SendMultiBlockMessage ( self, username, blocks ):
		msg = {
			"msgType":"assign",
			"username":username,
			"blocks":blocks
		}
		self.SendMessage( msg )

	def FreeBlock ( self, blockId ):
		if blockId in self.assignedBlocks:
			self.AddMissingBlockRange( blockId, blockId )
			del self.assignedBlocks[ blockId ]

	def FinishBlock ( self, blockId ):
		if blockId in self.assignedBlocks:
			del self.assignedBlocks[ blockId ]

	def GetWorkerOnBlock ( self, blockId ):
		if blockId in self.assignedBlocks:
			return self.assignedBlocks[ blockId ][0]

	def GetTimeOnBlock ( self, blockId ):
		if blockId in self.assignedBlocks:
			return self.assignedBlocks[ blockId ][1]

	def AddMissingSubmissions( self, startId, endId ):
		self.AddMissingBlockRange( int(startId/self.blockSize), int(endId/self.blockSize) )

	def RemoveFromMissing ( self, blockId ):
		for index in range( 0, len(self.missingBlocks) ):
			blocks = self.missingBlocks[index]

			if blockId >= blocks[0] and blockId <= blocks[1]:
				self.missingBlocks.pop( index )

				if blocks[1]>blockId+1:
					self.missingBlocks.insert( index, (blockId+1,blocks[1]) )
				if blocks[0]<blockId-1:
					self.missingBlocks.insert( index, (blocks[0],blockId-1) )
				break

	def DoCleanup ( self ):
		logging.info("Doing cleanup!")
		self.lastCleanup = time.time()
		kickTime = time.time() - self.maxBlockAllocationTime

		for blockId in list(self.assignedBlocks.keys()):
			if self.GetTimeOnBlock(blockId) <= kickTime:
				self.FreeBlock( blockId )

	def Update ( self ):
		while True:
			msg = self.RecvMessage()
			if msg:
				self.ReceivedMessage( msg )
			else:
				break
		
		time.sleep(0.05)
		if time.time() > self.lastCleanup+self.cleanupInterval:
			self.DoCleanup()

	def ReceivedMessage( self, msg ):

		username = msg.get("username")
		password = msg.get("password")

		# Error detection.
		if not VerifyMessage( msg ):
			logging.warning("Got bad message. Ignoring.")
			return False
		if not username in self.workers:
			logging.warning("Got msg from unknown source. Ignoring.")
			return False
		elif not self.workers.get(username)["pass"] == password:
			logging.warning("Got msg from source with wrong password. Ignoring.")
			return False

		version = msg.get("version")
		if (not version) or version < 4:
			return False

		msgType = msg.get("msgType")
		worker = self.workers.get(username)

		if msgType == "mappings" and self.GetWorkerOnBlock(msg.get("block"))==username:
			blockId, hashes = msg.get("block"), msg.get("hashes")
			self.ReceivedHashes( hashes )
			self.FinishBlock( blockId )
			logging.info("%s sent in some tags for block %i: %i/%i", username, blockId, len(hashes), self.blockSize )

		elif msgType == "request" and worker["lastRequest"]<time.time()+30:
			worker["lastRequest"] = time.time()
			self.HandleRequest(msg)

		elif msgType == "unping":
			print("Got unping from",username,"is version",msg.get("version"))

		elif msgType == "goodbye":
			if "reason" in msg:
				logging.info( "%s disconnected, reason: %s", username, msg.get("reason"))
			else:
				logging.info( "%s disconnected, with no given reason.", username)

		elif msgType == "badBlock":
			info = msg.get("info")
			logging.error("%s reported an issue with submission %i in block %i: %s", username, info[1], info[0], info[2] )
			self.badBlocks.append( info )
			self.FinishBlock( info[0] )

	def HandleRequest ( self, msg ):
		username = msg.get("username")
		blocks = []
		alreadyAssigned = self.GetBlocksAssignedToWorker(username)

		blocks = list(alreadyAssigned)
		for i in range(self.assignedBlocksPrWorker-len(alreadyAssigned)):
			blockId = self.GetMissingBlockId()
			blocks.append( blockId )
			self.AssignBlock( username, blockId )
			
		self.SendMultiBlockMessage( username, blocks )
		
		logging.info("%s requested some blocks, and is getting: %s", username, repr(blocks)) 

	def ReceivedHashes ( self, hashList ):
		for hash_and_tags in hashList:
			self.database.AddMappings( hash_and_tags.get("hash"), hash_and_tags.get("tags") )

	def EmergencyStop ( self ):
		logging.critical("Major Problem!")
		self.network.Close( {"msgType":"serverError"} )
		
	def SendMessage ( self, msg ):
		return self.network.SendMessage( msg )

	def RecvMessage ( self ):
		return self.network.RecvMessage()

	def GetBlocksAssignedToWorker( self, workerName ):
		return [ blockId for blockId in self.assignedBlocks if self.assignedBlocks[blockId][0] == workerName ]

	def SaveState ( self ):
		data = {
			"workers": self.workers,
			"missingBlocks": self.missingBlocks,
			"assignedBlocks": self.assignedBlocks,
			"badBlocks": self.badBlocks
		}
		
		jsonStr = json.dumps( data, indent=4 )

		with open("serverState.json","w") as f:
			f.write( jsonStr )

	def LoadState ( self ):
		if not os.path.isfile("serverState.json"):
			return False

		jsonStr = None
		with open("serverState.json","r") as f:
			jsonStr = f.read()

		if len(jsonStr) == 0:
			return False

		data = json.loads( jsonStr )

		self.workers = data["workers"]
		self.missingBlocks = data["missingBlocks"]
		self.badBlocks = data["badBlocks"]
		self.assignedBlocks = {}
		for i in data["assignedBlocks"]:
			self.assignedBlocks[ int(i) ] = data["assignedBlocks"][i]

		return True

	def Close ( self, msg={"msgType":"serverShutdown"} ):
		logging.info("Closing down")

		self.SendMessage( msg )
		self.network.Close()
		self.SaveState()

#################################

	@bf._availableToFrontend(False)
	def AddMissingBlockRange( self, startBlockId, endBlockId ):
		self.missingBlocks.append( [startBlockId,endBlockId] )

	@bf._availableToFrontend(False)
	def AddWorker ( self, username, password ):
		"""
		Adds another worker to the list.
		Arguments: username, password
		"""
		self.workers[username] = {
			"pass": password,
			"lastRequest": 0
		}

	@bf._availableToFrontend(True)
	def GetWorkers ( self ):
		"""
		Returns list of workers.
		"""
		return self.workers

	@bf._availableToFrontend(True)
	def GetMissingBlocks ( self ):
		"""
		Returns the currently missing blocks.
		"""
		return self.missingBlocks

	@bf._availableToFrontend(False)
	def RotateAssignedBlocks ( self, workerId=None ):
		"""
		Deassignes all blocks.
		"""
		list_ = []
		for blockId in list(self.assignedBlocks.keys()):
			if workerId == None or self.assignedBlocks[blockId][0] == workerId:
				self.FreeBlock( blockId )

	@bf._availableToFrontend(True)
	def GetAssignedBlocks ( self ):
		"""
		Returns the currently assigned blocks.
		"""
		return self.assignedBlocks

	@bf._availableToFrontend(True)
	def GetBadBlocks ( self ):
		"""
		Returns list of ids for blocks reported as bad.
		"""
		return [i[0] for i in self.badBlocks]

	@bf._availableToFrontend(False)
	def DumpBadBlocks ( self ):
		"""
		Prints errors from bad blocks to a file in a nice format.
		"""
		l = []
		for badBlock in self.badBlocks:
			e = len(badBlock)==4 and badBlock[3] or "No traceback found."
			s = "{2} on submission {1} in block {0}:\n{3}\n\n".format( badBlock[0],badBlock[1],badBlock[2], e )
			l.append(s)
		with open("errors.log","w") as f:
			f.write( "".join(l) )

	@bf._availableToFrontend(False)
	def RotateBadBlocks ( self ):
		"""
		Moves bad blocks to normal block rotation.
		"""
		for badBlock in self.badBlocks:
			self.AddMissingBlockRange( badBlock[0], badBlock[0] )
		self.badBlocks = []

	@bf._availableToFrontend(False)
	def Help ( self, func=None ):
		"""
		Displays an help message.
		"""
		if not func:
			print("Hydrus Furaffinity Scraper, Server")
			print("Available Commands:")
			for attrName in dir(self):
				attr = getattr( self, attrName )
				if hasattr( attr, "interfaceAvailable" ):
					print( "\t", attrName )
			print("Type 'Help COMMAND', to get help about the individual command.")
		else:
			attr = hasattr( self, func ) and getattr( self, func )
			if attr:
				print( attr.__doc__ )
			else:
				print("Command not found")


	@bf._availableToFrontend(False)
	def Ping ( self, workerName ):
		msg = {
			"msgType":"ping",
			"username":workerName
		}
		self.SendMessage( msg )

if __name__ == "__main__":
	# Set up logFile
	logging.basicConfig(filename='server.log',level=logging.INFO)

	# define a Handler which writes INFO messages or higher to the sys.stderr
	console = logging.StreamHandler()
	console.setLevel(logging.INFO)
	
	# set a format which is simpler for console use
	formatter = logging.Formatter('%(levelname)-8s %(asctime)-4s : %(message)s')

	# tell the handler to use this format
	console.setFormatter(formatter)

	# add the handler to the root logger
	logging.getLogger('').addHandler(console)

	bestServer = FurAffinityScraperServer("tcp://*:32123","tcp://*:12321")
	if bestServer.LoadState():
		logging.info("Previous server state found, loading...")
	else:
		logging.info("No previous configuration could be found. Creating 'serverState.json'.")
		logging.info("Please configure the config file, and then launch the server again.")
		bestServer.AddWorker("WORKER_NAME_HERE","WORKER_PASS_HERE")
		bestServer.AddMissingSubmissions(0,15000000)
		bestServer.Close()
		exit()

	# Now create the CLI frontend
	thread = bf.BackendThread(bestServer,"inproc://backendThread")
	thread.start()

	interface = bf.CLIFrontend("inproc://backendThread")
	interface.Start()
