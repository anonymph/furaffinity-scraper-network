"""
Provides classes for interacting with the Furaffinity website.
Currently only works with the default style.

Classes:
	Furaffinity
	FuraffinityPage

Nonstandard Libraries required:
	Requests
	BeautifulSoup 4
"""

import hashlib		# For hashing stuff.
import re			# For finding stuff in the soup.
import datetime		# For understanding time stuff.
import urllib		# For quoting url.
import itertools	# For count

import requests		# For accessing pages over the internet.
import bs4			# For traversing the responses.

MONTHS = {
	"january": 1,
	"february": 2,
	"march": 3,
	"april": 4,
	"may": 5,
	"june": 6,
	"july": 7,
	"august": 8,
	"september": 9,
	"october": 10,
	"november": 11,
	"december": 12
}

ADULT_MAP = [
	"general",
	"adult",
	"mature"
]

#################################

# Useful regexseses

NAME_UPLOADER_REGEX = re.compile(r'(.+) by (.+) --')

regex = {
	"fileName":		re.compile(r"[0-9]+\.(.+?.*)"),
	"downloadLink":	re.compile(r"Download"),
	"datetime":		re.compile(r"(.*?) (..?)(?:st|nd|rd|th), (....) (..):(..) (..)"),
		
	"category":		re.compile(r"Category:"),
	"theme":		re.compile(r"Theme:"),
	"species":		re.compile(r"Species:"),
	"gender":		re.compile(r"Gender:"),
	"favorites":	re.compile(r"Favorites:"),
	"comments":		re.compile(r"Comments:"),
	"views":		re.compile(r"Views:"),
	
	"keywords":		re.compile(r"/search/@keywords .+"),
	"ageRating":	re.compile(r"[^ ]*"),

	"bannedIP":		re.compile(r"Your IP address has been banned."),
	"notAllowed":	re.compile(r"You are not allowed to view this image."),
	"listNoSubs": 	re.compile(r"There are no submissions to list"),
	"watchlist":	re.compile(r"/unwatch/(.*)/\?key=[0-9a-f]*"),
	"extension":	re.compile(r".*\.(.*?)$")
}

##################################

def toUnicode( text ):
	"""
	Takes a string an makes it into a lowercase unicode object. Possibly also removing some unnessary text.
	"""

	newText = unicode( text ).encode('ascii', 'replace').lower()
	newText = re.match( r'^[\s,]*(.*)[\s,]*$', newText ).group(1)
	return newText

#################################

class TimezoneEst(datetime.tzinfo):
	"""
	Timezone seemingly used by furaffinity.
	This class does not know about summertime. Fuck summertime.
	TODO: Outphase this. It does not work with all timezones, which can be set by the user...
	"""

	def __init__(self):
		self.__offset = datetime.timedelta(hours = -5)
		self.__name = "EST"

	def utcoffset(self, dt):
		return self.__offset

	def tzname(self, dt):
		return self.__name

	def dst(self, dt):
		return None

#################################

# Useful errors

class LoginError (Exception):
	"""
	Could not login to the server.
	"""
	pass

class NotLoggedInError (Exception):
	"""
	Cannot properly interact with furaffinity unless you are logged in.
	"""
	pass

class MaturityError (Exception):
	"""
	Cannot access that submission on this account, because of maturity filter.
	Disable the filter and try again.
	"""
	pass

class NonExistantSubmissionError (Exception):
	"""
	Could not find that submission. It has either been taken down, or does not exist yet.
	"""
	pass

class AccessError (Exception):
	"""
	For some unknown reason furaffinity would not allow access to a submission. Try again.
	"""
	pass

class SubmissionFileNotAccessible (Exception):
	"""
	Could not find/download the submission file.
	"""
	pass
	pass

class IpBanError (Exception):
	"""
	You appear to be IP banned. Tough luck.
	"""
	pass

#################################

class Furaffinity:
	"""
	Represents furaffinity.
	"""

	def __init__ ( self, username=None, password=None, useragent=None, altSession=None ):
		"""
		Initiates the Furaffinity object. You can pass username, password and
		useragent to this if you want.
		"""
		self.session = altSession or requests.Session()
		self.loggedIn = False

		if useragent:
			self.SetUseragent(useragent)
		if username and password:
			self.Login( username, password )

	def SetUseragent( self, useragent ):
		"""
		Sets the useragent of the Furaffinity object. Best done before logging
		into the site.
		"""
		self.useragent = useragent
		self.session.headers["User-Agent"] = useragent

	def Login ( self, username, password, useragent=None ):
		"""
		Logs into furaffinty with the surplied username and password.
		"""

		if useragent:
			self.SetUseragent(useragent)
		
		self.username = username
		self.password = password

		data={
			"action":"login",
			"retard_protection":"1",
			"name":username,
			"pass":password,
			"login":"Login to FurAffinity"
		}
		response = self.session.post( "https://www.furaffinity.net/login/", data=data )

		if self.IsLoggedIn():
			self.loggedIn = True
			return True
		else:
			raise LoginError("Bad username and/or password")

	def IsLoggedIn ( self ):
		"""
		Returns whether you are logged in or not.
		"""
		response = self.session.get( "https://www.furaffinity.net/" )
		soup = bs4.BeautifulSoup(response.text)
		linkToUserpage = soup.find("a", id="my-username")

		return not not linkToUserpage

	#############################
	# Getting and setting settings down under.

	def GetAccountSettings ( self ):
		"""
		Returns a dict of account settings for the currently logged in account.
		
		Returns the following:
		- fullname
		- useremail
		- timezone
		- timezone_dst
		- bdayday
		- bdaymonth
		- bdayyear
		- viewmature
		- style
		- stylesheet

		Cannot return the following:
		- account_disabled (Is not informed by the webpage.)
		"""
		if not self.loggedIn:
			raise NotLoggedInError

		response = self.session.get( "https://www.furaffinity.net/controls/settings/" )
		soup = bs4.BeautifulSoup(response.text)

		oldSettings = {
			"fullname":			soup.find("input", attrs={"name": "fullname"}).get("value"),
			"useremail":		soup.find("input", attrs={"name": "useremail"}).get("value"),
			"timezone":			soup.find("select", attrs={"name": "timezone"}).find("option",selected="selected").get("value"),
			#"timezone_dst":		soup.find("input", attrs={"name": "timezone_dst"}).get("checked") and "1" or None,
			
			"bdayday": 			soup.find("select", attrs={"name": "bdayday"}).find("option",selected="selected").get("value"),
			"bdaymonth":		soup.find("select", attrs={"name": "bdaymonth"}).find("option",selected="selected").get("value"),
			"bdayyear": 		soup.find("select", attrs={"name": "bdayyear"}).find("option",selected="selected").get("value"),

			"viewmature": 		soup.find("select", attrs={"name": "viewmature"}).find("option",selected="selected").get("value"),
			
			"style": 			soup.find("select", attrs={"name": "style"}).find("option",selected="selected").get("value"),
			"stylesheet": 		soup.find("select", attrs={"name": "stylesheet"}).find("option",selected="selected").get("value")
		}

		#oldSettings["timezone_dst"]

		return oldSettings

	def SetAccountSettings ( self, **kwargs ):
		"""
		Sets some account settings.

		Arguments / Settings:
		- fullname: any string
		- useremail: any string
		- timezone: string formatted as "+1000" or simular.
		- timezone_dst: "0" (no) or "1" (yes)
		- bdayday: number as string
		- bdaymonth: number as string
		- bdayyear: number as string
		- viewmature: "0" (general),   (general, mature, adult) or "2" (general, mature)
		- style: "default"/"beta"
		- stylesheet: string
		- newpassword: string (be carefull with this, there is no confirmation)
		- account_disabled: "0" (no) or "1" (yes)
		"""

		if not self.loggedIn:
			raise NotLoggedInError

		settings = self.GetAccountSettings()

		for key, value in kwargs.items():
			settings[ key ] = value

		settings["do"] = "update"
		settings["oldpassword"] = self.password

		#if "timezone_dst" in kwargs:
		#	settings["timezone_dst"] = kwargs["timezone_dst"]=="1" and kwargs["timezone_dst"] or None

		if "newpassword" in kwargs:
			settings["newpassword"] = kwargs["newpassword"]
			self.password = kwargs["newpassword"]

		response = self.session.post( "https://www.furaffinity.net/controls/settings/", data=settings )
		return response

	def GetSiteSettings ( self ):
		"""
		Returns a dict of site settings for the currently logged in account.
		
		Returns the following:
		- disable_avatars
		- date_format
		- perpage
		- newsubmissions_direction
		- thumbnail_size
		- hide_favorites
		- no_guests
		- no_notes
		"""
		if not self.loggedIn:
			raise NotLoggedInError

		response = self.session.get( "https://www.furaffinity.net/controls/site-settings/" )
		soup = bs4.BeautifulSoup(response.text)

		oldSettings = {
			"disable_avatars": 			soup.find("input", id="disable_avatars_yes").get("checked") and "1" or "0",
			"date_format": 				soup.find("input", id="switch-date-format-full").get("checked") and "1" or "0",

			"perpage": 					soup.find("select", id="select-preferred-perpage").find("option",selected="selected").get("value"),
			"newsubmissions_direction": soup.find("select", id="select-newsubmissions-direction").find("option",selected="selected").get("value"),
			"thumbnail_size": 			soup.find("select", id="select-thumbnail-size").find("option",selected="selected").get("value"),

			"hide_favorites": 			soup.find("select", id="hide-favorites").find("option",selected="selected").get("value"),
			"no_guests": 				soup.find("select", id="no-guests").find("option",selected="selected").get("value"),
			"no_notes": 				soup.find("select", id="no-notes").find("option",selected="selected").get("value")
		}

		return oldSettings

	def SetSiteSettings ( self, **kwargs ):
		"""
		Sets some site settings.
		NOTE: You NEED to use every single arguement. It is not currently
			possible to only set a few settings, you need to set them all.

		Arguments / Settings:
		- disable_avatars: "0" (no) or "1" (yes)
		- date_format: "fuzzy", "full"
		- perpage: "24", "36", "48", "60"
		- newsubmissions_direction: "asc", "desc" 
		- thumbnail_size: "100", "150", "200"
		- hide_favorites: "n" ("Show everything"), "a" ("Hide adult"), "ma" ("Hide adult, mature"), "e" ("Hide everything")
		- no_guests: "0" (no) or "1" (yes)
		- no_notes: "0" (no) or "1" (yes)
		"""

		if not self.loggedIn:
			raise NotLoggedInError

		settings = self.GetSiteSettings()

		for key, value in kwargs.items():
			settings[ key ] = value

		settings["do"] = "update"
		settings["save_settings"] = "Save+Settings"

		response = self.session.post( "https://www.furaffinity.net/controls/site-settings/", data=settings )

	#####################################
	## Scraping stuff down under

	def GetPage ( self, pageId ):
		"""
		Returns FuraffinityPage object representing the submission page with
		the passed pageId. Will raise an error if it cannot access the
		submission.
		"""
		if not self.loggedIn:
			raise NotLoggedInError

		response = self.session.get( "https://www.furaffinity.net/view/"+str(pageId)+"/" )
		soup = bs4.BeautifulSoup(response.text)
		furPage = FuraffinityPage( soup )
		
		furPage.CheckForAccessErrors()

		return furPage

	def __GetUserSubmissions ( self, subPage, username ):
		"""
		Returns list of users submissions in an arbitrary subpage as pageIds.
		Should only be used internally.
		"""
		if not self.loggedIn:
			raise NotLoggedInError

		list_ = []
		username = username.lower()
		
		galleryPage = 0
		while True:
			galleryPage += 1

			response = self.session.get( "https://www.furaffinity.net/{}/{}/{}".format(subPage,username,galleryPage) )
			soup = bs4.BeautifulSoup(response.text)

			if soup.find("div",id="no-images"):
				break
			else:
				submissions = soup.find_all("b", class_="t-image")
				for submission in submissions:
					pageId = int(submission.get("id")[4:])
					list_.append(pageId)

		return list_

	def GetUserGallery ( self, username ):
		"""
		Returns list of submissions in an users gallery as pageIds.
		"""
		return self.__GetUserSubmissions( "gallery", username )

	def GetUserScraps ( self, username ):
		"""
		Returns list of submissions in an users scraps as pageIds.
		"""
		return self.__GetUserSubmissions( "scraps", username )

	def GetUserSubmissions ( self, username ):
		"""
		Returns list of submissions in an users gallery and scraps as pageIds.
		"""
		l = self.GetUserGallery(username)
		l.extend( self.GetUserScraps(username) )
		return l

	def GetUserFavorites ( self, username ):
		"""
		Returns list of submissions in an users favorites as pageIds.
		"""
		return self.__GetUserSubmissions( "favorites", username )

	def GetNewSubmissions ( self, nuke=False ):
		"""
		Returns list of submissions from the new submissions page.
		"""
		if not self.loggedIn:
			raise NotLoggedInError

		response = self.session.get( "https://www.furaffinity.net/msg/submissions/old/")
		soup = bs4.BeautifulSoup(response.text)

		list_ = []

		if soup.find(text=regex["listNoSubs"]):
			return list_

		while True:
					
			submissions = soup.find_all("b", class_="t-image")
			for submission in submissions:
				pageId = int(submission.get("id")[4:])
				list_.append(pageId)
	
			# Find next page button
			nextPageLink = soup.find("a", class_="more").get("href")
			if nextPageLink.find("old@")>=0:
				break
			
			response = self.session.get( "https://www.furaffinity.net" + nextPageLink )
			soup = bs4.BeautifulSoup(response.text)

		if nuke:
			self.NukeNewSubmissions()

		return list_

	def NukeNewSubmissions ( self ):
		"""
		Uses the 'nuke all submissions' buttion in the 'messagecenter'.
		This function does not seem to work.
		"""
		if not self.loggedIn:
			raise NotLoggedInError

		print("Nuking submissions")
		data = {
			"messagecenter-action": "Nuke+all+Submissions"
		}
		response = self.session.post( "https://www.furaffinity.net/msg/submissions/", data=data )

	def GetSearchResults ( self, searchQuery, timeRange="all", ratings=[1,1,1], types=[1,1,1,1,1,1] ):
		"""
		Uses the sites search page to search for submissions.
		Arguments:
			searchQuery - The string to search for.
			timeRange - The range to time to search within. (day,3days,week,all,month)
			ratings - Which ratings to include in the search. [General,Mature,Adult]
			types - Which types to include. [Art,Flash,Photo,Music,Story,Poetry]
		"""

		list_ = []
	
		form = {
			"q": searchQuery,
			"page": "0",
			"perpage": "60",			# More efficient than getting more pages from the server.
			"order-by": "relevancy",	# Funny enough, this is a completely irrelevant option for us.
			"order-direction": "desc",	# Again, irrelevant
			"range": timeRange,
			
			"rating-general": ratings[0] and "on" or None,
			"rating-mature": ratings[1] and "on" or None,
			"rating-adult": ratings[2] and "on" or None,

			"type-art": types[0] and "on" or None,
			"type-flash": types[1] and "on" or None,
			"type-photo": types[2] and "on" or None,
			"type-music": types[3] and "on" or None,
			"type-story": types[4] and "on" or None,
			"type-poetry": types[5] and "on" or None,

			"mode": "extended"			# Used to allow stuff like searching for tags (see below).
		}

		while True:
			form["page"] = str(int(form["page"]) + 1)
			response = self.session.post( "https://www.furaffinity.net/search/", data=form )
			soup = bs4.BeautifulSoup(response.text)
			#form["next_page"] = ">>>+24+more+>>>"

			submissions = soup.find_all("b", class_="t-image")
			if not submissions:
				break

			for submission in submissions:
				pageId = int(submission.get("id")[4:])
				list_.append(pageId)

		return list_

	def GetTaggedSubmissions ( self, tag, onlyImages=True ):
		"""
		Uses the sites search page to return the ids of submissions assigned the tag.
		"""
		if onlyImages:
			return self.GetSearchResults("@keywords+"+tag,types=[1,1,1,0,0,0])
		else:
			return self.GetSearchResults("@keywords+"+tag)

	def GetWatchlist ( self ):
		"""
		Returns a list of usernames, of the users on the logged in users watchlist.
		"""
		if not self.loggedIn:
			raise NotLoggedInError

		users = []

		for i in itertools.count(1):
			response = self.session.get( "https://www.furaffinity.net/controls/buddylist/"+str(i))
			soup = bs4.BeautifulSoup(response.text)

			for user in soup.find_all( "a", href=regex["watchlist"] ):
				username = toUnicode( regex["watchlist"].match(user.get("href")).group(1) )
				if not username in users:
					users.append( username )
				else:
					return users


#################################

class FuraffinityPage:
	"""
	Represents a single furaffinity page.
	"""

	timeOffset = TimezoneEst()	# This is exactly why I like prototype OOP better.

	def __init__ ( self, soup ):
		"""
		Initiates the FuraffinityPage object based on a soup object passed to it.
		"""
		self.localImagePath = None
		self.soup = soup

	def Download ( self, imagePath ):
		"""
		Downloads the submission to the specified location.
		The imagepath is relative to the execution folder and should not
		contain an extension, that is handeled by the method.
		"""
		try:
			r = requests.get(self.GetDownloadUrl(),stream=True)
		except requests.ConnectionError:
			raise SubmissionFileNotAccessible

		f = open( imagePath+'.'+self.GetExtension(), "wb" )
		for chunk in r.iter_content(chunk_size=1024): 
			if chunk:
				f.write(chunk)
		f.close()

	def GetRawData ( self ):
		"""
		Returns the raw binary data of the submission.
		Not effiecient when downloading. Use the Download method instead.
		"""
		r = requests.get(self.GetDownloadUrl())
		return r.raw

	def GetTitle ( self ):
		"""
		Returns the title of the submission in lowercase unicode.
		"""
		title = NAME_UPLOADER_REGEX.match(self.soup.title.string).group(1)
		return title
		
	def GetUploader ( self ):
		"""
		Returns the uploader of the submission in lowercase unicode.
		"""
		uploader = NAME_UPLOADER_REGEX.match(self.soup.title.string).group(2)
		return uploader

	def GetDownloadUrl ( self ):
		"""
		Returns the content url of the submission in lowercase unicode.
		"""
		imagePath = "https:" + urllib.quote(self.soup.find("a", text=regex['downloadLink']).get("href").encode('utf-8') )
		return  imagePath

	def GetExtension ( self ):
		"""
		Returns extension of the submission.
		"""
		link = self.soup.find("a", text=regex['downloadLink']).get("href")
		return regex["extension"].match(link).group(1)

	def GetFileName ( self ):
		"""
		Returns the file name of the submission, extracted from the url, in lowercase unicode.
		"""
		imagePath = self.GetDownloadUrl()
		imageFileName = None
		for i in re.findall( regex['fileName'], imagePath):
			imageFileName = i
		return imageFileName

	def GetHash ( self ):
		"""
		Retrieves the sha256 hash off the submission.

		If the image have been downloaded, and the object knows about this, it will retrieve the hash that way.
		Else it will need to download the submission, and hash it as it downloads.
		"""

		hash_ = hashlib.sha256()

		if self.localImagePath:
			with open( self.localImagePath, "rb" ) as f:
				hash_.update( f.read() )
		else:
			try:
				imagePath = self.GetDownloadUrl()
				r = requests.get(imagePath,stream=True)
			except requests.ConnectionError:
				raise SubmissionFileNotAccessible

			for chunk in r.iter_content(chunk_size=1024): 
				if chunk:
					hash_.update(chunk)

		return hash_.digest()

	def GetRawTime ( self ):
		"""
		Returns the time the submission was posted, as a string, in lowercase unicode.
		NOTE: This is gotten directly from the image information, unparsed.
		"""
		timeContainer = self.soup.find( "span", class_="popup_date")
		time = timeContainer.string
		if time[-3:] == "ago":
			time = timeContainer.get("title")
		return toUnicode(time)

	def GetDatetime ( self ):
		"""
		Returns a UTS datetime object with timezone information and everything.
		"""
		timeStr = self.GetRawTime()
		(monthStr, dayStr, yearStr, hourStr, minuteStr, PM_AM ) = regex['datetime'].match( timeStr ).group(1,2,3,4,5,6)
		hour = int(hourStr)%12 + (PM_AM=="pm" and 12 or 0)
		month = int(MONTHS[monthStr])
		timeObj = datetime.datetime(int(yearStr),month,int(dayStr),hour,int(minuteStr),tzinfo=self.timeOffset)
		return (timeObj - timeObj.utcoffset()).replace(tzinfo=self.timeOffset)

	def GetUploadTime ( self ):
		"""
		Retrieves the last-modified string from the header, as a string in UNIX timeformat.
		NOTE: You need to have parsed the image file before retrieving this.
		"""
		timeObj = self.GetDatetime()
		timeStr = timeObj.strftime("%d/%m/%Y %H:%M")
		return toUnicode(timeStr)
	
	def GetCategory ( self ):
		"""
		Returns the category of the submission, in lowercase unicode.
		"""
		category = self.soup.find("b", text=regex["category"]).next_sibling
		return toUnicode(category)
		
	def GetTheme ( self ):
		"""
		Returns whatever theme is, from the submission, in lowercase unicode.
		"""
		theme = self.soup.find("b", text=regex["theme"]).next_sibling
		return toUnicode(theme)

	def GetSpecies ( self ):
		"""
		Returns the species depicted in the submission, in lowercase unicode.
		"""
		species = self.soup.find("b", text=regex["species"])
		if species:
			return toUnicode(species.next_sibling)

	def GetGender ( self ):
		"""
		Returns the gender depicted in the submission, in lowercase unicode.
		"""
		gender = self.soup.find("b", text=regex["gender"])
		if gender:
			return toUnicode(gender.next_sibling)

	def GetFavorites ( self ):
		"""
		Returns the current amount of favorites the submission has, as an int.
		"""
		favs = self.soup.find("b", text=regex["favorites"]).next_sibling
		return int(favs)
		
	def GetComments ( self ):
		"""
		Returns the current amount of comments the submission has, as an int.
		"""
		comments = self.soup.find("b", text=regex["comments"]).next_sibling
		return int(comments)
		
	def GetViews ( self ):
		"""
		Returns the current amount of views the submission has, as an int.
		"""
		views = self.soup.find("b", text=regex["views"]).next_sibling
		return int(views)

	def GetKeywords ( self ):
		"""
		Returns the keywords list of the submission in lowercase unicode.
		"""
		keywords = []

		for a in self.soup.find_all( "a", href=regex["keywords"] ):
			keywords.append( toUnicode(a.string) )
	
		return keywords

	def GetAgeRating ( self ):
		"""
		Returns the age rating of the submission in lowercase unicode.
		"""
		ageRating = regex["ageRating"].search( self.soup.find("div",align="center").img.get("alt") ).group(0)
		return toUnicode(ageRating)

	def CheckForAccessErrors ( self ):
		"""
		Returns a string or none based on the access error or lack thereof.
		"""
		if self.soup.title.string == "System Error":
			raise NonExistantSubmissionError
		elif self.soup.find(text=regex["bannedIP"]):
			raise IpBanError
		elif self.soup.find(text=regex["notAllowed"]):
			sfw_toggle = self.soup.find("li",id="sfw-toggle")
			if (not sfw_toggle) or sfw_toggle.get("class").find("active"):
				raise MaturityError
			else:
				raise AccessError
			
	def GetTaggedUsers ( self ):
		"""
		Retrieves the users mentioned in the description of the submission.
		"""
		#Looks for "iconusername" and "linkusername" link classes and retrieve the username off of the link.
		taggedUsers = []
		for user in self.soup.find_all("a", class_="iconusername"):
			taggedUsers.append( toUnicode(user.get('href')[6:]) )
		for user in self.soup.find_all("a", class_="linkusername"):
			taggedUsers.append( toUnicode(user.get('href')[6:]) )

		return taggedUsers

	def GetHTML( self ):
		return self.soup.prettify()
